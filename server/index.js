var path = require('path')
    , express = require('express')
    , router = express.Router()
    , i18n = require('i18n-express')
    , bodyParser = require('body-parser')
    , compression = require('compression')
    , partials = require('express-partials')
    , cookieParser = require('cookie-parser');

var allConfigs = require('common/config/env');

module.exports = {
    start: function (app, config, entry) {
        app.engine('.html', require('ejs').__express);
        app.set('view engine', 'ejs');
        app.set('views', path.join(__dirname, config.viewPath));

        app.use(express.static(path.join(__dirname, config.assets)));

        app.use(partials());

        app.use(cookieParser());

        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({
            extended: true
        }));

        app.use(compression({
            filter: function (req, res) {
                return compression.filter(req, res)
            }
        }));

        app.use(i18n({
            translationsPath: path.join(__dirname, 'node_modules/common/i18n/locales'),
            siteLangs: ['en'],
            browserEnable: false,
            defaultLang: process.env.LNG || 'en'
        }));

        app.use('/', router);
        require(config.router)(router);

        if (entry) {
            app.listen(allConfigs.port, function () {
                console.log('Express server listening on port', allConfigs.port);
            });
        }
    }
};
